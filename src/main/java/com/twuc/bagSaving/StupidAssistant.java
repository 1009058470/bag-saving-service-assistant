package com.twuc.bagSaving;

import com.twuc.bagSaving.exception.InsufficientLockersException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StupidAssistant {
    private List<Cabinet> cabinets = new ArrayList<>();

    private final Map<BagSize, LockerSize> sizeMap = new HashMap<>();

    private void initSizeMap() {
        sizeMap.put(BagSize.BIG, LockerSize.BIG);
        sizeMap.put(BagSize.MEDIUM, LockerSize.MEDIUM);
        sizeMap.put(BagSize.SMALL, LockerSize.SMALL);
    }

    public StupidAssistant(Cabinet cabinet) {
        this.cabinets.add(cabinet);
        initSizeMap();
    }

    public StupidAssistant(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
        initSizeMap();
    }

    public Ticket save(Bag bag) {
        if (!sizeMap.containsKey(bag.getBagSize())) {
            throw new IllegalArgumentException("stupid assistant can't save this bag's size.");
        }
        boolean b = true;
        Ticket ticket = null;
        for (int i = 0; i < cabinets.size(); i++) {
            if (i == cabinets.size() - 1) {
                return cabinets.get(i).save(bag, sizeMap.get(bag.getBagSize()));
            }
            try {
                ticket = cabinets.get(i).save(bag, sizeMap.get(bag.getBagSize()));
            } catch (InsufficientLockersException e) {
                b = false;
            } finally {
                if (b) {
                    return ticket;
                }
            }
        }
        return ticket;
    }

    public Bag getBag(Ticket ticket) {
        return cabinets.get(0).getBag(ticket);
    }
}
