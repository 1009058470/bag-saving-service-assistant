package com.twuc.bagSaving.exception;

public class InsufficientLockersException extends RuntimeException {
    public InsufficientLockersException(String message) {
        super(message);
    }
}
