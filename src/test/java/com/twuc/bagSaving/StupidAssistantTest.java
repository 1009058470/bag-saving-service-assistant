package com.twuc.bagSaving;

import com.twuc.bagSaving.exception.InsufficientLockersException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithFullLockers;
import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

class StupidAssistantTest {

    @Test
    void should_return_ticket_when_save_by_assistant() {
        StupidAssistant assistant = new StupidAssistant(createCabinetWithPlentyOfCapacity());
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }

    @Test
    void should_return_bag_when_give_ticket_to_assistant() {
        StupidAssistant assistant = new StupidAssistant(createCabinetWithPlentyOfCapacity());
        Bag bag1 = new Bag(BagSize.MEDIUM);
        Ticket ticket = assistant.save(bag1);
        Bag bag2 = assistant.getBag(ticket);
        assertEquals(bag1,bag2);
    }

    @Test
    void should_throw_exception_when_space_full() {
        StupidAssistant assistant = new StupidAssistant(createCabinetWithFullLockers(new LockerSize[]{LockerSize.BIG}, 1));
        assertThrows(
                InsufficientLockersException.class,
                () -> assistant.save(new Bag(BagSize.BIG)));
    }


    @Test
    void should_save_bag_on_first_cabinet() {
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(createCabinetWithPlentyOfCapacity());
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }

    @Test
    void should_save_bag_on_forth_cabinet() {
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(createCabinetWithFullLockers(new LockerSize[]{LockerSize.MEDIUM},1));
        cabinets.add(createCabinetWithFullLockers(new LockerSize[]{LockerSize.MEDIUM},1));
        cabinets.add(createCabinetWithFullLockers(new LockerSize[]{LockerSize.MEDIUM},1));
        cabinets.add(createCabinetWithPlentyOfCapacity());
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }

}





































